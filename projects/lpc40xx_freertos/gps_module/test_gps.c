#include "Mockclock.h"
#include "Mockgpio.h"
#include "Mockqueue.h"
#include "Mockuart.h"
#include "gps.h"
#include "line_buffer.h"
#include "unity.h"
#include <string.h>

static line_buffer_s GPIO_line_buffer;
static char testing_of_buffer_memory[200];

void setUp(void) {
  uint32_t speed_of_clock = 0;
  gpio_s GPIO = {};
  clock__get_peripheral_clock_hz_ExpectAndReturn(speed_of_clock);
  uart__init_Expect(UART__3, speed_of_clock, 38400);
  gpio__construct_with_function_ExpectAndReturn(4, 28, 2, GPIO);
  gpio__construct_with_function_ExpectAndReturn(4, 29, 2, GPIO);

  QueueHandle_t testing_RXQ_handle, testing_TXQ_handle;
  xQueueCreate_ExpectAndReturn(50, sizeof(char), testing_RXQ_handle);
  xQueueCreate_ExpectAndReturn(80, sizeof(char), testing_TXQ_handle);
  uart__enable_queues_ExpectAndReturn(UART__3, testing_RXQ_handle, testing_TXQ_handle, 1);
  gps_init();
}
static void add_to_line_buffer(const char *string) {
  size_t count = 0;
  for (size_t index = 0; index < strlen(string); index++) {
    TEST_ASSERT_TRUE(line_buffer__add_byte(&GPIO_line_buffer, string[index]));
    count++;
  }
}
void test_line_buffer__gps(void) {
  line_buffer__init(&GPIO_line_buffer, testing_of_buffer_memory, sizeof(testing_of_buffer_memory));

  char *test_string = "$GPGGA,hhmmss.ss,37.3352,a,121.8811,a,x,xx,x.x,x.x,M,x.x,M,x.x,xxxx*hh\r\n";
  add_to_line_buffer(test_string);

  char NMEA_string[300];
  TEST_ASSERT_TRUE(line_buffer__remove_line(&GPIO_line_buffer, NMEA_string, sizeof(NMEA_string)));
  TEST_ASSERT_EQUAL_STRING("$GPGGA,hhmmss.ss,37.3352,a,121.8811,a,x,xx,x.x,x.x,M,x.x,M,x.x,xxxx*hh\r", NMEA_string);
}

void test_GPGLL_line_is_ignored(void) {

  const char *data_returned_from_uart = "$GPGLL,56.09872,N,82.098763,W,hhmmss.ss,A\r\n";
  for (size_t index = 0; index <= strlen(data_returned_from_uart); index++) {
    const bool last_char = (index < strlen(data_returned_from_uart));
    uart__get_ExpectAndReturn(UART__3, NULL, 0, last_char);
    uart__get_IgnoreArg_input_byte();
    uart__get_ReturnThruPtr_input_byte(&data_returned_from_uart[index]);
  }
  gps__run_once();

  GPS_Coordinates new_coordinates = gps__get_coordinates();
  float latitude = 56.09872;
  float longitude = 82.098763;

  TEST_ASSERT_EQUAL(true, new_coordinates.received);
  TEST_ASSERT_EQUAL(0, new_coordinates.latitude);
  TEST_ASSERT_EQUAL(0, new_coordinates.longitude);
}

void test_GPGGA_coordinates_are_parsed_or_not(void) {
  const char *data_returned_from_uart = "$GPGGA,hhmmss.ss,20.5937°,N,78.9629,W,x,xx,x.x,x.x,M,x.x,M,x.x,xxxx*hh\r\n";
  for (size_t index = 0; index <= strlen(data_returned_from_uart); index++) {
    const bool last_char = (index < strlen(data_returned_from_uart));
    uart__get_ExpectAndReturn(UART__3, NULL, 0, last_char);
    uart__get_IgnoreArg_input_byte();
    uart__get_ReturnThruPtr_input_byte(&data_returned_from_uart[index]);
  }
  gps__run_once();

  GPS_Coordinates new_coordinates = gps__get_coordinates();
  float latitude = 20.5937;
  float longitude = 78.9629;
  TEST_ASSERT_EQUAL(true, new_coordinates.received);
  TEST_ASSERT_EQUAL(latitude, new_coordinates.latitude);
  TEST_ASSERT_EQUAL(longitude, new_coordinates.longitude);
}
