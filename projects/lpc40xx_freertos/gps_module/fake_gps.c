#include "fake_gps.h"
#include "clock.h"
#include "gpio.h"
#include "uart.h"
#include "uart_printf.h"

static uart_e GPS_UART = UART__3;

void fake_gps__init(void) {
  uart__init(GPS_UART, clock__get_peripheral_clock_hz(), 38400);
  gpio__construct_with_function(GPIO__PORT_4, 28, GPIO__FUNCTION_2);
  gpio__construct_with_function(GPIO__PORT_4, 29, GPIO__FUNCTION_2);

  QueueHandle_t rxq_handle = xQueueCreate(40, sizeof(char));
  QueueHandle_t txq_handle = xQueueCreate(80, sizeof(char));
  uart__enable_queues(GPS_UART, rxq_handle, txq_handle);
}

void fake_gps__run_once(uint32_t value) {
  float latitude = value * 1.15;
  uart_printf(GPS_UART, "$GPGGA,hhmmss.ss,37.3352,a,121.8811,a,x,xx,x.x,x.x,M,x.x,M,x.x,xxxx*pp\r\n", latitude);
}
