#pragma once

#include <stdint.h>

void fake_gps__run_once(uint32_t value);
void fake_gps__init(void);
