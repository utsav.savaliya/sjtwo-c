#pragma once
#include "gps.h"
#include "clock.h"
#include "gpio.h"
#include "line_buffer.h"
#include "uart.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Change this according to which UART you plan to use
static const uart_e GPS_UART = UART__2;

static line_buffer_s line_buffer;
static char line_buffer_memory[600];

static GPS_Coordinates parsed_coordinates;

static void gps_transfer_data_from_uart_to_line_buffer() {
  char one_byte;
  const uint32_t time_out = 0;

  while (uart__get(GPS_UART, &one_byte, time_out)) {
    line_buffer__add_byte(&line_buffer, one_byte);
  }

  // for (int i = 0; i < strlen(accumulated_data); i++) {
  // line_buffer__add_byte(&line_buffer, accumulated_data[i]);
  // }
}

static void gps__parse_coordinates_from_line(void) {
  char gps_line[200];
  parsed_coordinates.received = false;

  if (line_buffer__remove_line(&line_buffer, gps_line, sizeof(gps_line))) {
    bool success = false;
    enum NEMA type_of_GPS = UNSUPPORTED;
    int i;

    printf("gps_line = %c", gps_line[0]);
    printf("%c", gps_line[1]);
    printf("%c", gps_line[2]);
    printf("%c", gps_line[3]);
    printf("%c", gps_line[4]);
    printf("%c\n", gps_line[5]);

    char type_of_nmea[7];
    memcpy(type_of_nmea, &gps_line[0], 6);
    type_of_nmea[6] = '\0';

    printf("type_of_nmea = %s\n", type_of_nmea);
    if (!strcmp(type_of_nmea, "$GPGLL")) {
      printf("type_of_nmea = %c\n", type_of_nmea[5]);
      type_of_GPS = GPGLL;
    }
    if (!strcmp(type_of_nmea, "$GPGGA")) {
      printf("type_of_nmea = %c\n", type_of_nmea[5]);
      type_of_GPS = GPGGA;
    }

    if (type_of_GPS == GPGGA) {
      char north_or_south = gps_line[25];
      char east_or_west = gps_line[36];

      if (((east_or_west == 'E') || (east_or_west == 'W')) && ((north_or_south == 'N') || (north_or_south == 'S'))) {
        success = true;
      } else {
        success = false;
      }

      if (success) {

        char latitude[8];
        char longitude[9];

        memcpy(latitude, &gps_line[17], 7);
        memcpy(longitude, &gps_line[27], 8);
        longitude[8] = '\0';
        latitude[7] = '\0';

        parsed_coordinates.longitude = (float)atof(longitude);
        parsed_coordinates.latitude = (float)atof(latitude);
      } else {
        parsed_coordinates.longitude = 0;
        parsed_coordinates.latitude = 0;
      }
    }

    if (type_of_GPS == GPGGA) {

      parsed_coordinates.longitude = 0;
      parsed_coordinates.latitude = 0;
      success = true;
      printf("success = %d\n", success);
    } else {
      parsed_coordinates.longitude = 0;
      parsed_coordinates.latitude = 0;
      success = false;
      printf("success = %d\n", success);
    }
    parsed_coordinates.received = success;
  }
}

static void gps_process_completed_lines(void) {
  char line[200] = {0};
  while (line_buffer__remove_line(&line_buffer, line, sizeof(line))) {
  }
}

void gps_init(void) {

  line_buffer__init(&line_buffer, line_buffer_memory, sizeof(line_buffer_memory));
  uart__init(GPS_UART, clock__get_peripheral_clock_hz(), 38400);

  QueueHandle_t RXQ_Handle = xQueueCreate(50, sizeof(char));
  QueueHandle_t TXQ_Handle = xQueueCreate(80, sizeof(char));
  uart__enable_queues(GPS_UART, RXQ_Handle, TXQ_Handle);
}

void gps__run_periodic(const char *accumulated_data) {
  gps_transfer_data_from_uart_to_line_buffer(accumulated_data);
  gps_process_completed_lines();
}
void gps__run_once(void) {
  gps_transfer_data_from_uart_to_line_buffer();
  gps__parse_coordinates_from_line();
}

GPS_Coordinates gps__get_coordinates(void) { return parsed_coordinates; }

void test_(void) { gps__run_periodic("GPGGA....\n"); }