#include "line_buffer.c"
#include "unity.h"

static line_buffer_s line_buffer;
static int line_buffer_memory[100];

#define SIZE_of_ARRAY(array) sizeof(array) / sizeof(array[0])

void setUp(void) {
  TEST_ASSERT_TRUE(line_buffer__init(&line_buffer, line_buffer_memory, SIZE_of_ARRAY(line_buffer_memory)));
  // wite box tetsting
  TEST_ASSERT_EQUAL(100, line_buffer.max_size);
}

void test_init_failure_NULL_module(void) {
  TEST_ASSERT_FALSE(line_buffer__init(NULL, line_buffer_memory, SIZE_of_ARRAY(line_buffer_memory)));
}

void test_init_failure_NULL_memory(void) {
  TEST_ASSERT_FALSE(line_buffer__init(&line_buffer, NULL, SIZE_of_ARRAY(line_buffer_memory)));
}

void test_single_line_removal(void) {
  TEST_ASSERT_TRUE(line_buffer__add_byte(&line_buffer, 'a'));
  TEST_ASSERT_TRUE(line_buffer__add_byte(&line_buffer, 'b'));
  TEST_ASSERT_TRUE(line_buffer__add_byte(&line_buffer, 'c'));
  TEST_ASSERT_TRUE(line_buffer__add_byte(&line_buffer, '\n'));

  char buffer[8] = {0};

  TEST_ASSERT_TRUE(line_buffer__remove_line(&line_buffer, buffer, sizeof(buffer)));
  TEST_ASSERT_EQUAL_STRING(buffer, "abc");
}

void test_simple_line_completed(void) {}

void test_multiple_line_removal(void) {
  TEST_ASSERT_TRUE(line_buffer__add_byte(&line_buffer, 'a'));
  TEST_ASSERT_TRUE(line_buffer__add_byte(&line_buffer, 'b'));
  TEST_ASSERT_TRUE(line_buffer__add_byte(&line_buffer, '\n'));

  TEST_ASSERT_TRUE(line_buffer__add_byte(&line_buffer, 'c'));
  TEST_ASSERT_TRUE(line_buffer__add_byte(&line_buffer, 'd'));
  TEST_ASSERT_TRUE(line_buffer__add_byte(&line_buffer, '\n'));

  char buffer[8] = {0};

  TEST_ASSERT_TRUE(line_buffer__remove_line(&line_buffer, buffer, sizeof(buffer)));
  TEST_ASSERT_EQUAL_STRING(buffer, "ab");

  TEST_ASSERT_TRUE(line_buffer__remove_line(&line_buffer, buffer, sizeof(buffer)));
  TEST_ASSERT_EQUAL_STRING(buffer, "cd");
}

void test_line_buffer_addbyte_when_buffer_full(void) { TEST_IGNORE_MESSAGE("There are some task needed to be done"); }
// verbose for the compiler to generate comment