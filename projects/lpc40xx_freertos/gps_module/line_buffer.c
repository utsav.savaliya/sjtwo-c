#pragma once
#include "line_buffer.h"
#include "string.h"
#include <stdio.h>

bool line_buffer__init(line_buffer_s *buffer, void *memory, size_t max_size_of_memory) {
  bool status = false;
  if (NULL != buffer && NULL != memory) {
    status = true;
    *buffer = (line_buffer_s){.memory = memory, .max_size = max_size_of_memory};
  }
  return status;
}

// Adds a byte to the buffer, and returns true if the buffer had enough space to add the byte

bool line_buffer__add_byte(line_buffer_s *buffer, char byte) {
  bool one_byte_added = false;

  if (buffer->write_index < buffer->max_size) {
    buffer->memory[buffer->write_index] = byte;
    ++(buffer->write_index);
    one_byte_added = true;
  }
  return one_byte_added;
}

bool line_buffer_private_new_line(line_buffer_s *buffer) { return (NULL != strchr(buffer->memory, '\n')); }

bool line_buffer__remove_line(line_buffer_s *buffer, char *line, size_t line_max_size) {

  bool line__removed = false;

  if (line_buffer_private_new_line(buffer)) {
    const char *newline_location = strchr(buffer->memory, '\n');
    const size_t bytes_to_copy = (newline_location - buffer->memory);

    if (bytes_to_copy < line_max_size) {
      memcpy(line, buffer->memory, bytes_to_copy);
      line__removed = true;
      char *destination = buffer->memory;
      const char *source = buffer->memory + bytes_to_copy + 1;
      const size_t bytes_to_shift_down = ((buffer->max_size) - bytes_to_copy);
      memcpy(destination, source, bytes_to_shift_down);
    }
  }
  return line__removed;
}