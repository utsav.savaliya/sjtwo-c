#pragma once
#include <stdbool.h>
#include <stdint.h>

typedef struct {
  float latitude;
  float longitude;
  bool received;
} GPS_Coordinates;

enum NEMA { GPGGA = 0, GPGLL, UNSUPPORTED };

void gps_init(void);
void gps__run_once(void);

GPS_Coordinates gps__get_coordinates(void);

static void gps__transfer_data_from_uart_to_line_buffer(void);
static void gps_process_completed_lines(void);
static void gps__parse_coordinates_from_line(void);

static bool gps__parse_nmea_string(char *data_receiver);
