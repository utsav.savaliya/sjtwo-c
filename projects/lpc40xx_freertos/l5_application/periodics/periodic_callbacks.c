#include "periodic_callbacks.h"
#include "board_io.h"
#include "clock.h"
#include "fake_gps.h"
#include "gpio.h"
#include "gps.h"
#include "uart.h"
#include <stdio.h>

static uint32_t counter = 0;

void periodic_callbacks__initialize(void) { gps_init(); }

void periodic_callbacks__1Hz(uint32_t callback_count) {
  gpio__toggle(board_io__get_led0());
  gps__run_once();

  GPS_Coordinates new_coordinates = gps__get_coordinates();
  if (new_coordinates.received) {
    printf("Lat:%f, Long:%f (%d)\n", new_coordinates.latitude, new_coordinates.longitude, counter);
  } else {
    printf("GPS coordinates received are not recieved.(%d)\n", counter);
  }
}

void periodic_callbacks__10Hz(uint32_t callback_count) {

  counter++;
  if (counter > 10000) {
    counter = 1;
  }
  gpio__toggle(board_io__get_led2()); // To show this is working.
  fake_gps__run_once(counter);
}
void periodic_callbacks__100Hz(uint32_t callback_count) {}

void periodic_callbacks__1000Hz(uint32_t callback_count) {}
