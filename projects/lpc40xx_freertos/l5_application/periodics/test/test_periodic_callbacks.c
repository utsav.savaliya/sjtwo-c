#include "Mockboard_io.h"
#include "Mockfake_gps.h"
#include "Mockgpio.h"
#include "Mockgps.h"
#include "Mockuart.h"
#include "clock.h"
#include "periodic_callbacks.h"
#include "unity.h"
#include <stdio.h>
#include <string.h>

void setUp(void) {}

void tearDown(void) {} //*

void test__periodic_callbacks__initialize(void) { //*
  gps_init_Expect();
  periodic_callbacks__initialize();
}

void test__periodic_callbacks__1Hz(void) { //*
  gpio_s GPIO = {};
  board_io__get_led0_ExpectAndReturn(GPIO);
  gpio__toggle_Expect(GPIO);
  gps__run_once_Expect();
  GPS_Coordinates new_coordinates;
  gps__get_coordinates_ExpectAndReturn(new_coordinates);
  periodic_callbacks__1Hz(0);
}

void test__periodic_callbacks__10Hz(void) {
  gpio_s GPIO = {};
  board_io__get_led2_ExpectAndReturn(GPIO);
  gpio__toggle_Expect(GPIO);
  fake_gps__run_once_Expect(1);
  periodic_callbacks__10Hz(0);
}